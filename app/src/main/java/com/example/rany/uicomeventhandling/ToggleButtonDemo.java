package com.example.rany.uicomeventhandling;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ToggleButton;

public class ToggleButtonDemo extends AppCompatActivity {

    ImageView img1;
    ToggleButton tgBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toggle_button_demo);

        img1 = findViewById(R.id.img1);
        tgBtn = findViewById(R.id.tgBtn);
        tgBtn.setText("Hide|Show");
        tgBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                    img1.setVisibility(View.VISIBLE);
                else
                    img1.setVisibility(View.GONE);
            }
        });

    }
}
