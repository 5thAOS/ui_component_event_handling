package com.example.rany.uicomeventhandling;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

public class AutoCompleteTextDemo extends AppCompatActivity {

    AutoCompleteTextView txtProvince;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auto_complete_text_demo);

        txtProvince = findViewById(R.id.txtProvince);

        final String[] provinces = {"Kompong Speu", "Kompong Cham", "Kompot", "Phnom Penh" +
                "Takeo", "Kandal"};

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_list_item_1, provinces);

        txtProvince.setAdapter(arrayAdapter);
        txtProvince.setThreshold(2);


    }
}
