package com.example.rany.uicomeventhandling;

import android.app.DatePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class DatePickerDemo extends AppCompatActivity implements DatePickerDialog.OnDateSetListener{

    TextView txtResult;
    Button btnDate;
    Calendar calendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_picker_demo);

        txtResult = findViewById(R.id.txtDate);
        btnDate = findViewById(R.id.btnDate);
        btnDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker(v);
            }
        });
    }
    void showDatePicker(View v){
        DateFragement dateFragement = new DateFragement();
        dateFragement.show(getFragmentManager(), "DatePicker");
    }
    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        calendar = new GregorianCalendar(year, month, dayOfMonth);
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.FULL);
        txtResult.setText(dateFormat.format(calendar.getTime()));
    }
}
