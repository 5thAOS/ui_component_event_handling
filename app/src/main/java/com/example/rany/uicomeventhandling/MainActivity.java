package com.example.rany.uicomeventhandling;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private RadioGroup rd;
    private RadioButton rdf, rdm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rd = findViewById(R.id.rd);
        rdf = findViewById(R.id.rdf);
        rdm = findViewById(R.id.rdm);

        rd.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.rdf :
                        Toast.makeText(MainActivity.this, "I am a girl", Toast.LENGTH_LONG).show();
                        break;
                    case R.id.rdm :{
                        Toast.makeText(MainActivity.this, "I am a boy", Toast.LENGTH_SHORT).show();
                        break;
                    }
                }
            }
        });

    }
}
