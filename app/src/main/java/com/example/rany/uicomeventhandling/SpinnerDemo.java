package com.example.rany.uicomeventhandling;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

public class SpinnerDemo extends AppCompatActivity {

    Spinner spinner;
    int check = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner_demo);

        spinner = findViewById(R.id.spinner);

        final String[] provinces = {"Kompong Speu", "Kompong Cham", "Kompot", "Phnom Penh",
                "Takeo", "Kandal", "--Choose--"};
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                this,android.R.layout.simple_spinner_dropdown_item, provinces){
            @Override
            public int getCount() {
                int count = super.getCount();
                return (count > 0)? (count -1) : count;
            }
        };

        spinner.setAdapter(arrayAdapter);
        spinner.setSelection(spinner.getCount());
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String choosen = provinces[position];
                Toast.makeText(SpinnerDemo.this, choosen, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(SpinnerDemo.this, "Please choose it", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
