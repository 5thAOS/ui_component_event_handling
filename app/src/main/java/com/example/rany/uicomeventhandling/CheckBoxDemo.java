package com.example.rany.uicomeventhandling;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

public class CheckBoxDemo extends AppCompatActivity {

    private CheckBox cb1, cb2, cb3, cb4, cb5;
    private Button btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_box_demo);

        cb1 = findViewById(R.id.cbHTML);
        cb2 = findViewById(R.id.cbCSS);
        cb3 = findViewById(R.id.cbJS);
        cb4 = findViewById(R.id.cbBS);
        cb5 = findViewById(R.id.cbjQ);
        btnSubmit = findViewById(R.id.btnSubmit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringBuilder str = new StringBuilder();
                str.append("You have select : "+"\n");
                if(cb1.isChecked())
                    str.append(cb1.getText().toString()+"\n");
                if(cb2.isChecked())
                    str.append(cb2.getText().toString()+"\n");
                if(cb3.isChecked())
                    str.append(cb3.getText().toString()+"\n");
                if(cb4.isChecked())
                    str.append(cb4.getText().toString()+"\n");
                if(cb5.isChecked())
                    str.append(cb5.getText().toString()+"\n");

                Toast.makeText(CheckBoxDemo.this, str.toString(), Toast.LENGTH_LONG).show();
            }
        });


    }
}
